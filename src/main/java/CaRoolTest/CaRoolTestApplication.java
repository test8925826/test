package CaRoolTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CaRoolTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaRoolTestApplication.class, args);
	}

}
