package CaRoolTest;

import java.time.LocalDate;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AnalysePneuController {

	@GetMapping("getAnalysePneu")
	public AnalysePneuOutput getAnalyse(@RequestParam float epaisseurGomme, @RequestParam boolean possedeClou,
			@RequestParam int nombreCraquelures) {

		AnalysePneuInput input = new AnalysePneuInput();
		input.epaisseurGomme = epaisseurGomme;
		input.possedeClou = possedeClou;
		input.nombreCraquelures = nombreCraquelures;

		float score = calculScore(input);

		return CalculOutPut(score);
	}

	private AnalysePneuOutput CalculOutPut(float score) {

		AnalysePneuOutput output = new AnalysePneuOutput();
		output.scoreEtatPneu = score;

		if (output.scoreEtatPneu >= 90) {
			output.validitePneu = true;
		} else {
			output.dateAnalyse = LocalDate.now();
		}
		return output;
	}

	private float calculScore(AnalysePneuInput input) {
		float score = 100;
		if (input.possedeClou) {
			score -= 60;
		}
		score -= input.nombreCraquelures * 10;
		score -= (input.epaisseurGomme - 3) * 3.5;
		return score;
	}

}

class AnalysePneuInput {

	public float epaisseurGomme;
	public boolean possedeClou;
	public int nombreCraquelures;

}

class AnalysePneuOutput {
	public float scoreEtatPneu;
	public boolean validitePneu;
	public LocalDate dateAnalyse;

}
