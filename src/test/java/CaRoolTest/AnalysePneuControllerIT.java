package CaRoolTest;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AnalysePneuControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void test() throws Exception {
        // Given
        // When
        MvcResult result = mockMvc.perform(get("/getAnalysePneu")
                        .param("epaisseurGomme", "5")
                        .param("possedeClou", "false")
                        .param("nombreCraquelures", "2"))
                .andExpect(status().isOk())
                .andReturn();
        // Then
        JSONAssert.assertEquals(result.getRequest().getContentAsString(), """
                {
                    "scoreEtatPneu":85.0,
                    "validitePneu":false,
                    "dateAnalyse":"2024-06-24"
                }
                """, true);
    }
}
