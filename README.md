# Faire une API REST Spring

## Use Case
Faire un path pour lancer une analyse d'un pneu d'une voiture.

## Input
- **épaisseur de la gomme** (de 0 à 8)
- **possède un clou**
- **nombre craquelures**

## Output
- **score état pneu**
- **validité du pneu**
- **date analyse**

## Règles de calcul du score du pneu
Le score du pneu est calculé de façon dégressive à partir de 100 avec les règles suivantes :
- **possède un clou**: score - 60 (si le pays de la voiture est France : - 50)
- **nombre craquelures**: score - (nombre craquelures * 10)
- **épaisseur**: score + (épaisseur - 3) * 3.5

## Validité du pneu
La validité du pneu varie selon le pays de la voiture :
- **France**: score >= 90
- **Pologne**: score > 80
- **Autres pays**: score > 85 (La liste des pays avec exception peut grandir)